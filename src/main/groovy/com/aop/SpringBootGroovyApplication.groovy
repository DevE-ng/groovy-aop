package com.aop

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringBootGroovyApplication {
    static String GLOBAL_URL = "http://10.200.88.111:8516/v1/camera/63d56cd3-5bf5-4263-974a-624c7f569a8a/"

    static void main(String[] args) {
        SpringApplication.run SpringBootGroovyApplication, args
    }
}
