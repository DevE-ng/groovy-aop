package com.aop.metrics

import java.util.concurrent.ConcurrentHashMap

class MetricsCollection {

    private static ConcurrentHashMap<String,Integer> metrics = new ConcurrentHashMap();

    static
    {
            MetricTypes.values().each {
                metrics[it] = 0;
            }
    }

    static void IncreaseFalls(metricName){
        metrics[metricName] += 1;
    }

    static void ZeroOutFall(metricName){
        metrics[metricName] = 0
    }

    static Integer FallsAmount(metricName){
        metrics[metricName]
    }
}
