package com.aop.service

import com.aop.SpringBootGroovyApplication
import org.springframework.beans.factory.annotation.Autowired


@org.springframework.stereotype.Service
class Service {

    @Autowired
    ServiceRequestingRest serviceThrowingExc

    String serveWithExceptionCatching() {
        try {
            String ret = serviceThrowingExc.MakeRequest(SpringBootGroovyApplication.GLOBAL_URL);

            ret
        } catch (Exception ex) {
           System.out.println("Exception has been handled")
           "FAIL"
        }
    }
}
